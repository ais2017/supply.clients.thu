package service;

import model.*;
import repository.ContractRepository;
import repository.EntityRepository;
import repository.ProductRepository;
import repositoryImpl.ContractRepositoryImpl;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Dmitry on 23.11.2017.
 */
public class ContractService {
    private ContractRepository contractRepository;
    private EntityRepository entityRepository;
    private ProductRepository productRepository;

    public ArrayList<Contract> getAllContracts(){
        return contractRepository.getAll();
    }

    public ContractService(ContractRepository contractRepository, EntityRepository entityRepository, ProductRepository productRepository){this.contractRepository = contractRepository; this.entityRepository = entityRepository; this.productRepository = productRepository;}


    public Contract prepareContract (String INN, long contractNumber, Manager manager, ArrayList<ProductInContract> productsInContract, double amount, Entity entity, Date deliveryDate){
        Contract contract;
        if(entityRepository.searchEntity(INN)){
            contract = new Contract(contractNumber, manager, productsInContract,amount,entity,deliveryDate);
        }else
            throw new  IllegalArgumentException("Такого юридического лица не существует");
        return contractRepository.save(contract);
    }


    public Contract terminateContract(Long id){
        Contract contract = contractRepository.getById(id);
        if(contract == null)
            throw new IllegalArgumentException("Контракт не найден");
        contract.terminateContract();
        return contractRepository.save(contract);
    }


    public Contract concludeContract(Long id){
        Contract contract  = contractRepository.getById(id);
        if(contract == null)
            throw new IllegalArgumentException("Контракт не найден");
        contract.concludeContract();
        return contractRepository.save(contract);
    }

    public ArrayList<Contract> getPaidContracts(){
        return contractRepository.getAll(ContractPaymentState.PAID);

    }


    public Contract addProduct(Long id, Product product, int count){
        Contract contract = contractRepository.getById(id);
        boolean prod = productRepository.searchProduct(product.getName());
        if(contract == null)
            throw new IllegalArgumentException("Контракт не найден");
        if(prod == false)
            throw new IllegalArgumentException("Продукт не найден");
        contract.addProduct(product, count);
        return contract;
    }



}
