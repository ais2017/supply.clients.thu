package service;

import model.Individual;
import repository.IndividualRepository;
import repository.Repository;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Dmitry on 13.12.2017.
 */
public class IndividualService {
    private IndividualRepository individualRepository;

    public IndividualService(IndividualRepository individualRepository){
        this.individualRepository = individualRepository;
    }

    public Collection<Individual> getAllIndividuals() {
        return individualRepository.getAll();
    }





}
