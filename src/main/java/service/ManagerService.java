package service;

import model.Manager;
import repository.ManagerRepository;

import java.util.Collection;

/**
 * Created by Dmitry on 14.12.2017.
 */
public class ManagerService {
    private ManagerRepository managerRepository;

    public ManagerService(ManagerRepository individualRepository){
        this.managerRepository = managerRepository;
    }

    public Collection<Manager> getAllManagers() {
        return managerRepository.getAll();
    }


}
