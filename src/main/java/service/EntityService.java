package service;

import model.Entity;
import repository.EntityRepository;
import repository.Repository;
import repositoryImpl.EntityRepositoryImpl;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Dmitry on 12.12.2017.
 */
public class EntityService {

    private EntityRepository entityRepository;

    public EntityService(EntityRepository entityRepository){
        this.entityRepository = entityRepository;
    }

    public Collection<Entity> getAllEntities() {
        return entityRepository.getAll();
    }

    public void addEntity(Entity entity){
        entityRepository.addEntity(entity);
    }

    public void editEntity(Long id, String companyName, String FIORepresentative, String INN, String requisites, long accountNumber, String address, String telephone){
        entityRepository.editEntity(id, companyName, FIORepresentative, INN, requisites, accountNumber,address,telephone);
    }


}
