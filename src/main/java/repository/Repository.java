package repository;

import java.util.Collection;

/**
 * Created by Dmitry on 23.11.2017.
 */
public interface Repository<T> {


    public Collection<T> getAll();


}
