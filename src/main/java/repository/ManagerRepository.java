package repository;



import model.Manager;

import java.util.Collection;

/**
 * Created by Dmitry on 14.12.2017.
 */
public interface ManagerRepository {

    public Collection<Manager> getAll();

}
