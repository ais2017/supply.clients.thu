package repository;


import model.Individual;

import java.util.Collection;

/**
 * Created by Dmitry on 13.12.2017.
 */
public interface IndividualRepository {
    public Collection<Individual> getAll();
    public void addIndividual (Individual individual);

}
