package repository;

import model.Entity;

import java.util.Collection;

/**
 * Created by Dmitry on 13.12.2017.
 */
public interface EntityRepository {
    public Collection<Entity> getAll();
    public void addEntity (Entity entity);

    public boolean searchEntity(String INN);

    public void editEntity(Long id, String companyName, String FIORepresentative, String INN, String requisites, Long accountNumber, String address, String telephone);


}
