package repository;

import model.Contract;
import model.ContractPaymentState;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Dmitry on 23.11.2017.
 */
public interface ContractRepository extends Repository<Contract> {
    public ArrayList<Contract> getAll ();

    public ArrayList <Contract> getAll(ContractPaymentState contractPaymentState);

    public Contract getById(Long id);

    public Contract save(Contract contract);

}
