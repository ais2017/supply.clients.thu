package repository;

import model.Product;

import java.util.ArrayList;

/**
 * Created by Dmitry on 16.12.2017.
 */
public interface ProductRepository {
    public ArrayList<Product> getAll();
    public boolean searchProduct(String name);
}
