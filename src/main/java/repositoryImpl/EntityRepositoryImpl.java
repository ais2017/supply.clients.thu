package repositoryImpl;

import model.Entity;
import repository.EntityRepository;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Dmitry on 13.12.2017.
 */
public class EntityRepositoryImpl implements EntityRepository {
    private static AtomicLong entityId = new AtomicLong(0);
    public static Long incrementAndGetId(){
        return entityId.incrementAndGet();
    }
    public static Map<Long, Entity> entities = new HashMap<>();
    public static String FIRM_NAME = "Газпром";
    public static String FIRM_FIO = "Петр Петрович Петров";
    public static String FIRM_INN = "500100732259";
    public static String FIRM_REQUISITES = "40702810000320001374 БИК 044525219 к/с № 30101810500000000219";
    public static Long FIRM_ACCOUNT_NUMBER = 40702810038200536L;
    public static String FIRM_ADDRESS = "улица Ленина, д. 5";
    public static String FIRM_TELEPHONE = "84992324587";
    @Override
    public ArrayList<Entity> getAll() {
        ArrayList<Entity> entityArraList = new ArrayList<>();
        for(Entity entity:entities.values())
            entityArraList.add(entity);
        return entityArraList;
    }

    @Override
    public void addEntity(Entity entity) {
        long newKey = incrementAndGetId();
        entities.put(newKey, entity);
    }

    @Override
    public void editEntity(Long id, String companyName, String FIORepresentative, String INN, String requisites, Long accountNumber, String address, String telephone) {
        Entity entity = entities.get(id);
        entity.setAccountNumber(accountNumber);
        entity.setAddress(address);
        entity.setCompanyName(companyName);
        entity.setFIORepresentative(FIORepresentative);
        entity.setINN(INN);
        entity.setRequisites(requisites);
        entity.setTelephone(telephone);

    }

    @Override
    public boolean searchEntity(String INN) {
        for(Entity entity:entities.values()){
            if (entity.getINN().equals(INN))
                return true;
        }
        return false;
    }

    public static Entity generateEntity(){

        long newKey = incrementAndGetId();
        Entity newVal = new Entity(FIRM_NAME, FIRM_FIO, FIRM_INN, FIRM_REQUISITES, FIRM_ACCOUNT_NUMBER, FIRM_ADDRESS, FIRM_TELEPHONE);
        entities.put(newKey, newVal);
        return newVal;
    }
}