package repositoryImpl;

import model.Individual;
import repository.IndividualRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Dmitry on 13.12.2017.
 */
public class IndividualRepositoryImpl implements IndividualRepository{
    private static AtomicLong individualId = new AtomicLong(0);
    public static Long incrementAndGetId(){
        return individualId.incrementAndGet();
    }
    public static Map<Long, Individual> individuals = new HashMap<>();
    public static String CLIENT_FIO = "Иван Иванович Иванов";
    public static String CLIENT_PHONE = "89178549357";
    public static String CLIENT_ADDRESS = "улица Пушкина, д.2 кв 5";
    public static String CLIENT_EMAIL = "ivanovii@gmail.com";

    @Override
    public Collection<Individual> getAll() {
        ArrayList<Individual> individualArraList = new ArrayList<>();
        for(Individual individual:individuals.values())
            individualArraList.add(individual);
        return individualArraList;
    }

    @Override
    public void addIndividual(Individual individual) {
        long newKey = incrementAndGetId();

        individuals.put(newKey, individual);

    }

    public static Individual generateIndividual(){
        long newKey = incrementAndGetId();
        Individual newVal = new Individual(CLIENT_FIO,CLIENT_PHONE,CLIENT_ADDRESS,CLIENT_EMAIL);
        individuals.put(newKey, newVal);
        return newVal;
    }
}
