package repositoryImpl;

import model.Product;
import model.ProductInContract;

/**
 * Created by Dmitry on 14.12.2017.
 */
public class ProductInContractRepositoryImpl {

    public static Product PRODUCT_IN_CONTRACT = ProductRepositoryImpl.generateProduct();
    public static int COUNT = 35;

    public static ProductInContract generateProductInContract(){
        ProductInContract productInContract = new ProductInContract(PRODUCT_IN_CONTRACT, COUNT);
        return productInContract;
    }
}
