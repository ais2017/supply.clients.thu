package repositoryImpl;

import model.Manager;

import repository.ManagerRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Dmitry on 13.12.2017.
 */
public class ManagerRepositoryImpl implements ManagerRepository {

    public static String MANAGER_FIO = "Петров Илья Владимирович";
    public static String MANAGER_ROLE = "Начальник отдела продаж";

    public static Map<Long, Manager> managers = new HashMap<>();
    private static AtomicLong managerId = new AtomicLong(0);
    public static Long incrementAndGetId(){
        return managerId.incrementAndGet();
    }


    public static Manager generateManager(){
        Manager manager = new Manager(MANAGER_FIO, MANAGER_ROLE);
        return manager;
    }

    @Override
    public Collection<Manager> getAll() {
        ArrayList<Manager> managerArrayList = new ArrayList<>();
        for(Manager manager:managers.values())
            managerArrayList.add(manager);
        return managerArrayList;
    }
}
