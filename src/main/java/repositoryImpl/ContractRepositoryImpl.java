package repositoryImpl;

import model.*;
import repository.ContractRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Dmitry on 13.12.2017.
 */
public class ContractRepositoryImpl implements ContractRepository {

    public static Manager MANAGER = ManagerRepositoryImpl.generateManager();

    public static ArrayList<ProductInContract> PRODUCTS = new ArrayList<>();
    public static double AMOUNT = 150000.50;
    public static Entity ENTITY = EntityRepositoryImpl.generateEntity();
    public static Date CONCLUSION_DATE = new Date();
    public static Date DELIVERY_DATE = new Date();


    public static Map<Long, Contract> contracts = new HashMap<>();
    private static AtomicLong contractId = new AtomicLong(0);
    public static Long incrementAndGetId(){
        return contractId.incrementAndGet();
    }

    @Override
    public ArrayList<Contract> getAll(ContractPaymentState contractPaymentState) {
        ArrayList contractsByPaymentState = new ArrayList<>();
        contracts.values().forEach(v -> {
            if (v.getPaymentState().equals(contractPaymentState))
                contractsByPaymentState.add(v);
        });
        return contractsByPaymentState;
    }

    @Override
    public ArrayList<Contract> getAll() {
        ArrayList<Contract> contractArrayList = new ArrayList<>();
        for(Contract contract:contracts.values())
            contractArrayList.add(contract);
        return contractArrayList;
    }

    @Override
    public Contract save(Contract contract) {
        contracts.put(contract.getContractNumber(),contract);
        return contract;
    }

    @Override
    public Contract getById(Long id) {
        return contracts.get(id);
    }

    public static Contract generateContract(){
        long newKey = incrementAndGetId();
        PRODUCTS.add(ProductInContractRepositoryImpl.generateProductInContract());
        Contract contract = new Contract(newKey, MANAGER, PRODUCTS, AMOUNT, ENTITY, DELIVERY_DATE);
        contracts.put(newKey, contract);

        return contract;
    }


    public static Collection <Contract> generateContractCollection(){
        List<Contract> contractArrayList = new ArrayList<>();
        Contract notConcludedContract = generateContract();
        contractArrayList.add(notConcludedContract);
        contracts.put(incrementAndGetId(), notConcludedContract);

        Contract concludedContract = generateContract();
        concludedContract.concludeContract();
        contractArrayList.add(concludedContract);
        contracts.put(incrementAndGetId(), concludedContract);


        concludedContract.terminateContract();
        contractArrayList.add(concludedContract);
        contracts.put(incrementAndGetId(), concludedContract);

        return contractArrayList;

    }
}
