package repositoryImpl;

import model.Product;
import repository.ProductRepository;
import repository.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Dmitry on 14.12.2017.
 */
public class ProductRepositoryImpl implements ProductRepository{

    public static Map<Long, Product> products = new HashMap<>();

    private static String PRODUCT_NAME = "Мультиварка";
    private static double PRODUCT_PRICE = 3850.50;

    private static String PRODUCT_NAME2 = "Микроволновка";
    private static double PRODUCT_PRICE2 = 4735.50;

    private static AtomicLong individualId = new AtomicLong(0);
    public static Long incrementAndGetId(){
        return individualId.incrementAndGet();
    }




    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> productArrayList = new ArrayList<>();
        for(Product product:products.values())
            productArrayList.add(product);
        return productArrayList;
    }

    @Override
    public boolean searchProduct(String name) {
        for(Product product:products.values()){
            if (product.getName().equals(name))
                return true;
        }
        return false;
    }

    public static Product generateProduct2(){
        long newKey = incrementAndGetId();

        Product product = new Product(PRODUCT_NAME2, PRODUCT_PRICE2);
        products.put(newKey, product);
        return product;

    }

    public static Product generateProduct(){

        long newKey = incrementAndGetId();

        Product product = new Product(PRODUCT_NAME, PRODUCT_PRICE);
        products.put(newKey, product);
        return product;
    }

}
