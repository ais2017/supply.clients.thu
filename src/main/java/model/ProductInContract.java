package model;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class ProductInContract {
    private Product product;
    private int count;

    public ProductInContract(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void incCount(int count){
        this.count+=count;
    }

    public void decCount(int count){
        this.count-=count;
    }

}
