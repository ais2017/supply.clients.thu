package model;

/**
 * Created by Dmitry on 23.11.2017.
 */
public enum ContractPaymentState {
    PAID("Оплачено"),
    UNPAID("Не опалчено");
    protected String value;

    ContractPaymentState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
