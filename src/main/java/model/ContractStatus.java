package model;

/**
 * Created by Dmitry on 23.11.2017.
 */
public enum ContractStatus {
    CONCLUDED("Заключен"),
    NOT_CONCLUDED("Не заключен"),
    TERMINATED("Расторгнут");
    protected String value;

    ContractStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
