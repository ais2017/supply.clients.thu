package model;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class Manager {
    private String FIO;
    private String role;

    public Manager(String FIO, String role) {
        this.FIO = FIO;
        this.role = role;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
