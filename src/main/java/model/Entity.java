package model;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class Entity {
    private String companyName;
    private String FIORepresentative;
    private String INN;
    private String requisites;
    private long accountNumber;
    private String address;
    private String telephone;

    public Entity(String companyName, String FIORepresentative, String INN, String requisites, long accountNumber, String address, String telephone) {
        this.companyName = companyName;
        this.FIORepresentative = FIORepresentative;
        this.INN = INN;
        this.requisites = requisites;
        this.accountNumber = accountNumber;
        this.address = address;
        this.telephone = telephone;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

    public String getFIORepresentative() {
        return FIORepresentative;
    }

    public void setFIORepresentative(String FIORepresentative) {
        this.FIORepresentative = FIORepresentative;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


}
