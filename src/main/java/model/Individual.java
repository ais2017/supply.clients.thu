package model;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class Individual {
    private String FIO;
    private String telephone;
    private String address;
    private String email;

    public Individual(String FIO, String telephone, String address, String email) {
        this.FIO = FIO;
        this.telephone = telephone;
        this.address = address;
        this.email = email;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
