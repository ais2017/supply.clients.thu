package model;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class Product {
    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Product product = (Product) obj;
        return name.equals(product.name)
                &&price==((Product) obj).price;
    }


    @Override
    public int hashCode() {
        int hash = 37;
        hash = hash*17 + name.hashCode();
        hash = hash*17 +(int) price;

        return hash;
    }
}
