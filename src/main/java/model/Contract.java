package model;


import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class Contract {
    private long contractNumber;
    private Manager manager;
    private ContractStatus status;
    private ArrayList<ProductInContract> productsInContract;
    private double amount;
    private Entity entity;
    private Date conclusionDate;
    private Date deliveryDate;
    private ContractPaymentState paymentState;

    //public static Contract createContract()

    public Contract(long contractNumber, Manager manager, ArrayList<ProductInContract> productsInContract, double amount, Entity entity,  Date deliveryDate) {
        this.contractNumber = contractNumber;
        this.manager = manager;
        this.status = ContractStatus.NOT_CONCLUDED;
        this.productsInContract = productsInContract;
        this.amount = amount;
        this.entity = entity;
        this.conclusionDate = null;
        this.deliveryDate = deliveryDate;
        this.paymentState = ContractPaymentState.UNPAID;
    }

    public long getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(long contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public ContractStatus getStatus() {
        return status;
    }

//    public void setStatus(String status) {
//        this.status = status;
//    }

    public ArrayList<ProductInContract> getProductsInContract() {
        return productsInContract;
    }

    public void setProductsInContract(ArrayList<ProductInContract> productsInContract) {
        this.productsInContract = productsInContract;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public Date getConclusionDate() {
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public ContractPaymentState getPaymentState() {
        return paymentState;
    }

//    public void setPaymentState(String paymentState) {
//        this.paymentState = paymentState;
//    }


    public void addProduct(Product product, int count){
        boolean find = false;
        for (int i = 0;i<this.productsInContract.size();i++){
            if(this.productsInContract.get(i).getProduct().equals(product)){
                find = true;
                ProductInContract incProduct = new ProductInContract (this.productsInContract.get(i).getProduct(),this.productsInContract.get(i).getCount()+count);
                this.productsInContract.set(i,incProduct);
            }
        }
        if (!find){
            ProductInContract newProduct = new ProductInContract(product, count);
            this.productsInContract.add(newProduct);
        }
    }

    public void concludeContract(){
        if(this.status==ContractStatus.CONCLUDED)
            throw new IllegalStateException("Договор уже заключен");
        this.status = ContractStatus.CONCLUDED;
        this.paymentState = ContractPaymentState.PAID;
        this.setConclusionDate(new Date());

    }

    public void terminateContract(){
        if(this.status==ContractStatus.TERMINATED)
            throw new IllegalStateException("Договор уже расторгнут");
        if(this.status==ContractStatus.NOT_CONCLUDED)
            throw new IllegalStateException("Договор еще не заключен");
        this.status = ContractStatus.TERMINATED;
    }
}
