package modelTest;

import model.ProductInContract;
import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.ProductInContractRepositoryImpl;

/**
 * Created by Dmitry on 15.12.2017.
 */
public class ProductInContractTest {
    @Test
    public void test() {

        ProductInContract productInContract = ProductInContractRepositoryImpl.generateProductInContract();

        Assert.assertEquals(productInContract.getCount(),ProductInContractRepositoryImpl.COUNT);
        Assert.assertEquals(productInContract.getProduct(), ProductInContractRepositoryImpl.PRODUCT_IN_CONTRACT);

    }

    @Test
    public void incTest(){
        ProductInContract productInContract = ProductInContractRepositoryImpl.generateProductInContract();

        productInContract.incCount(7);
        Assert.assertEquals(productInContract.getCount(), 42);

    }

    @Test
    public void decCount(){
        ProductInContract productInContract = ProductInContractRepositoryImpl.generateProductInContract();

        productInContract.decCount(10);
        Assert.assertEquals(productInContract.getCount(), 25);
    }

}
