package modelTest;

import model.Manager;
import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.ManagerRepositoryImpl;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class ManagerTest {
    @Test
    public void test() {

        Manager manager = ManagerRepositoryImpl.generateManager();

        Assert.assertEquals(manager.getFIO(), ManagerRepositoryImpl.MANAGER_FIO);
        Assert.assertEquals(manager.getRole(), ManagerRepositoryImpl.MANAGER_ROLE);

    }

}
