package modelTest;

import model.Individual;
import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.IndividualRepositoryImpl;

/**
 * Created by Dmitry on 15.12.2017.
 */
public class IndividualTest {

    @Test
    public void test() {

        Individual individual = IndividualRepositoryImpl.generateIndividual();


        Assert.assertEquals(individual.getFIO(),IndividualRepositoryImpl.CLIENT_FIO);
        Assert.assertEquals(individual.getTelephone(),IndividualRepositoryImpl.CLIENT_PHONE);
        Assert.assertEquals(individual.getAddress(), IndividualRepositoryImpl.CLIENT_ADDRESS);
        Assert.assertEquals(individual.getEmail(),IndividualRepositoryImpl.CLIENT_EMAIL);





    }


}
