package modelTest;

import model.Entity;
import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.EntityRepositoryImpl;

/**
 * Created by Dmitry on 15.12.2017.
 */
public class EntityTest {
    @Test
    public void test() {

        Entity entity = EntityRepositoryImpl.generateEntity();

        //Assert.assertEquals(entity.getAccountNumber(), EntityRepositoryImpl.FIRM_ACCOUNT_NUMBER);
        Assert.assertEquals(entity.getAddress(), EntityRepositoryImpl.FIRM_ADDRESS);
        Assert.assertEquals(entity.getCompanyName(),EntityRepositoryImpl.FIRM_NAME);
        Assert.assertEquals(entity.getFIORepresentative(), EntityRepositoryImpl.FIRM_FIO);
        Assert.assertEquals(entity.getINN(), EntityRepositoryImpl.FIRM_INN);
        Assert.assertEquals(entity.getRequisites(), EntityRepositoryImpl.FIRM_REQUISITES);
        Assert.assertEquals(entity.getTelephone(), EntityRepositoryImpl.FIRM_TELEPHONE);




    }

}
