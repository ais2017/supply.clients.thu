package modelTest;

import model.*;
import org.junit.Assert;
import repositoryImpl.ContractRepositoryImpl;
import org.junit.Test;
import repositoryImpl.ProductRepositoryImpl;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class ContractTest {

    private static final double DELTA = 1e-15;

    @Test
    public void test() {


        Contract contract = ContractRepositoryImpl.generateContract();
        Assert.assertEquals(contract.getPaymentState(), ContractPaymentState.UNPAID);
        Assert.assertEquals(contract.getEntity(), ContractRepositoryImpl.ENTITY);
        Assert.assertEquals(contract.getAmount(), ContractRepositoryImpl.AMOUNT, DELTA);
        //Assert.assertEquals(contract.getConclusionDate(), ContractRepositoryImpl.CONCLUSION_DATE);
        Assert.assertEquals(contract.getDeliveryDate(), ContractRepositoryImpl.DELIVERY_DATE);
        Assert.assertEquals(contract.getProductsInContract(), ContractRepositoryImpl.PRODUCTS);
        Assert.assertEquals(contract.getManager(), ContractRepositoryImpl.MANAGER);
        Assert.assertEquals(contract.getStatus(), ContractStatus.NOT_CONCLUDED);
    }

    @Test
    public void testConcluded() {
        Contract contract = ContractRepositoryImpl.generateContract();
        contract.concludeContract();

        Assert.assertEquals(contract.getStatus(), ContractStatus.CONCLUDED);
    }

    @Test(expected = IllegalStateException.class)
    public void testConcludedBad() {
        Contract contract = ContractRepositoryImpl.generateContract();
        contract.concludeContract();

        contract.concludeContract();
    }


    @Test
    public void testTerminate() {
        Contract contract = ContractRepositoryImpl.generateContract();
        contract.concludeContract();
        contract.terminateContract();

        Assert.assertEquals(contract.getStatus(), ContractStatus.TERMINATED);
    }

    @Test(expected = IllegalStateException.class)
    public void testTerminateBad() {
        Contract contract = ContractRepositoryImpl.generateContract();
        contract.terminateContract();

    }

    @Test
    public void testAddProduct() {
        Contract contract = ContractRepositoryImpl.generateContract();
        Product product = ProductRepositoryImpl.generateProduct();

        contract.addProduct(product, 5);

        Assert.assertEquals(contract.getProductsInContract().get(0).getCount(), 40);
    }
///добавление товара, которого не было в договоре
    @Test
    public void testAddProduct2() {

        Contract contract = ContractRepositoryImpl.generateContract();
        Product product = ProductRepositoryImpl.generateProduct2();

        contract.addProduct(product, 5);
        //образуется ArrrayList, так как товара еще нет в договоре
        Assert.assertEquals(contract.getProductsInContract().get(1).getCount(), 5);
    }

    }

