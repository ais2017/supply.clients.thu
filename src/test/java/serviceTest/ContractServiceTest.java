package serviceTest;

import model.*;
import org.junit.Assert;
import org.junit.Test;
import repositoryImpl.*;
import service.ContractService;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Dmitry on 16.11.2017.
 */
public class ContractServiceTest {
    private ContractService contractService = new ContractService(new ContractRepositoryImpl(), new EntityRepositoryImpl(), new ProductRepositoryImpl());
    private ContractRepositoryImpl contractRepository = new ContractRepositoryImpl();
    private EntityRepositoryImpl entityRepository = new EntityRepositoryImpl();
    private ProductRepositoryImpl productRepository = new ProductRepositoryImpl();
    @Test
    public void prepareContract(){
        Entity entity = entityRepository.generateEntity();
        ArrayList<ProductInContract>  productsInContract = new ArrayList<>();
        productsInContract.add(ProductInContractRepositoryImpl.generateProductInContract());

        contractService.prepareContract("500100732259",0L,ManagerRepositoryImpl.generateManager(), productsInContract, 38500.50, entity, new Date());

        ArrayList<Contract> contracts = contractRepository.getAll();

        Contract chosenContract = contracts.get(0);


        Assert.assertEquals(chosenContract.getEntity().getINN(),entity.getINN());


    }
    @Test(expected = IllegalArgumentException.class)
    public void prepareContractBad(){
        Entity entity = entityRepository.generateEntity();
        ArrayList<ProductInContract>  productsInContract = new ArrayList<>();
        productsInContract.add(ProductInContractRepositoryImpl.generateProductInContract());


        contractService.prepareContract("5000732259",0L,ManagerRepositoryImpl.generateManager(), productsInContract, 38500.50, entity, new Date());

        ArrayList<Contract> contracts = contractRepository.getAll();

        Contract chosenContract = contracts.get(0);

        Assert.assertEquals(chosenContract.getEntity().getINN(),entity.getINN());


    }


    @Test
    public void concludeContract(){
        contractRepository.generateContract();
        ArrayList<Contract> contracts = contractService.getAllContracts();
//        Contract notConcludedContract = contracts.get(0);
        System.out.println(contracts.get(0).getStatus());
        contractService.concludeContract(0L);
        Assert.assertEquals(contracts.get(0).getStatus(), ContractStatus.CONCLUDED);
    }

    @Test
    public void terminateContract(){
        contractRepository.generateContract();
        ArrayList<Contract> contracts = contractService.getAllContracts();
        contractService.concludeContract(0L);
        contractService.terminateContract(0L);
        Assert.assertEquals(contracts.get(0).getStatus(), ContractStatus.TERMINATED);

    }

    @Test
    public void addProductTest(){
        contractRepository.generateContract();
        ArrayList<Contract> contracts = contractService.getAllContracts();
        Product product = productRepository.generateProduct();
        contractService.addProduct(0L, product, 5);
        Assert.assertEquals(contracts.get(0).getProductsInContract().get(0).getCount(), 40);

    }




    @Test
    public void viewPaidContract(){
        Entity entity = entityRepository.generateEntity();
        contractRepository.generateContractCollection();
        ArrayList<Contract> contracts = contractService.getPaidContracts();

        Contract chosenContract = contracts.get(0);
        Assert.assertEquals(chosenContract.getPaymentState(), ContractPaymentState.PAID);

    }




}
