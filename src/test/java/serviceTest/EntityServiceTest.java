package serviceTest;

import model.Entity;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.TestComponent;
import repositoryImpl.EntityRepositoryImpl;
import service.EntityService;

import java.util.ArrayList;

/**
 * Created by Dmitry on 15.12.2017.
 */
public class EntityServiceTest {

    private EntityService entityService = new EntityService(new EntityRepositoryImpl());
    private EntityRepositoryImpl entityRepository = new EntityRepositoryImpl();

    Entity entity = EntityRepositoryImpl.generateEntity();
    ArrayList<Entity> entities = entityRepository.getAll();

    Entity chosenEntity = entities.get(0);


    @Test
    public void addEntity() {

        entityService.addEntity(entity);


        Assert.assertEquals(entity.getAccountNumber(), chosenEntity.getAccountNumber());
        Assert.assertEquals(entity.getAddress(), chosenEntity.getAddress());
        Assert.assertEquals(entity.getCompanyName(),chosenEntity.getCompanyName());
        Assert.assertEquals(entity.getFIORepresentative(), chosenEntity.getFIORepresentative());
        Assert.assertEquals(entity.getINN(), chosenEntity.getINN());
        Assert.assertEquals(entity.getRequisites(), chosenEntity.getRequisites());
        Assert.assertEquals(entity.getTelephone(), chosenEntity.getTelephone());

    }

    @Test
    public void editEntity(){

        entityService.editEntity(0L, "Зил", "Зигмунд Зигмундович Зигмундов", "73991382193812931", "83230283901238901", 73213982173981L, "Улица Мира д. 5","88005353535");

        Assert.assertEquals(chosenEntity.getAccountNumber(), 73213982173981L);
        Assert.assertEquals(chosenEntity.getAddress(), "Улица Мира д. 5");
        Assert.assertEquals(chosenEntity.getCompanyName(),"Зил");
        Assert.assertEquals(chosenEntity.getFIORepresentative(), "Зигмунд Зигмундович Зигмундов");
        Assert.assertEquals(chosenEntity.getINN(), "73991382193812931");
        Assert.assertEquals(chosenEntity.getRequisites(), "83230283901238901");
        Assert.assertEquals(chosenEntity.getTelephone(), "88005353535");

    }

}
