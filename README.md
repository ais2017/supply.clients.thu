# Описание организации
Организация осуществляет поставку товаров мелкой электроники оптом и в розницу. Работа с юридическими лицами осуществляется по договору. Для физических лиц возможна выдача заказов в пунктах самовывоза. Организация имеет сеть складов и собственную службу доставки.
# Описание области автоматизации
Система должна помогать менеджерам отдела продаж работать с клиентами.